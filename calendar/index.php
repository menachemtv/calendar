<?php

require __DIR__ . '/vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secrets.json');
$client->addScope('https://www.googleapis.com/auth/calendar');
if(isset ($_POST['start'])):
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  $startDate=$_POST['startDate'];
  $endDate=$_POST['endDate'];
  $startTime=$_POST['startTime'];
  $endTime=$_POST['endTime'];
  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

  $event = new Google_Service_Calendar_Event(array(
  'summary' => 'test event 2017',
  'location' => 'Jerusalem',
  'description' => 'Testing the google calendar API',
  'start' => array(
    'dateTime' => $_POST['start'].':00+02:00',
    'timeZone' => 'Asia/Jerusalem',
  ),
  'end' => array(
  'dateTime' => $_POST['end'].':00+02:00',
    'timeZone' => 'Asia/Jerusalem',
  ),
  'recurrence' => array(
    'RRULE:FREQ=DAILY;COUNT=1'
  ),
  'attendees' => array(
    array('email' => 'nochaz@walla.co.il'),
    array('email' => 'sbrin@example.com'),
  ),
  'reminders' => array(
    'useDefault' => FALSE,
    'overrides' => array(
      array('method' => 'email', 'minutes' => 24 * 60),
      array('method' => 'popup', 'minutes' => 10),
    ),
  ),
));
  
$calendarId = 'primary';
$event = $service->events->insert($calendarId, $event);
printf('Event created: %s\n', $event->htmlLink);
  
} else {
  $redirect_uri =  'http://menachemtv.myweb.jce.ac.il/calendar/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));

}
else:
?>

<html>
<head>
  <meta charset="utf-8">
  <title>Google Calendar</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--<link rel="icon" type="image/x-icon" href="favicon.ico">-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<form method="POST" action="">
	<h5>set date and time to meeting</h5> 
			<div class="form-group">
				<label for="start">Start Event</label>
				<input type="datetime-local" class="form-control" name="start">
			</div>
			<div class="form-group">
				<label for="end">End Evnet</label>
				<input type="datetime-local" class="form-control" name="end">
			</div>
	<input type="submit" name="submit" value="submit" class="btn btn-success">
	<button type="reset" class="btn button">Clean</button>
</form>


</body>
</html>
 
<?php endif; ?>

